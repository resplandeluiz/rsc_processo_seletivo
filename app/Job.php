<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = ['name'];

    public function proposal()
    {
        return $this->belongsToMany('App\ProposalJob', 'proposal_jobs', 'proposal_id');
    }

}
