<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;

class JobController extends Controller
{


    public function index()
    {
        return Job::paginate(10);
    }


    public function store(Request $request)
    {
        return Job::create($request->all());
    }
}
