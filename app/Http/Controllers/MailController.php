<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Job;
use Mail;

class MailController extends Controller
{
    public function basic_email($proposals)
    {

        $client = Client::find($proposals[0]->client_id);
        $msg = '';
        foreach($proposals as $proposal){              
            $job = Job::find($proposal->job_id);
            $msg = $job->name.', '.$msg;
         }
         $data = array('name' => $client->name, 'body' => $msg);
       
        $to_name = 'Proposta recebida!';
        $to_email = $client->email;
      

        Mail::send('mail.index', $data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject(' Proposta#, recebida com sucesso!');
            $message->from('SEU-EMAIL', 'PropostaService');
        });
    }
}
