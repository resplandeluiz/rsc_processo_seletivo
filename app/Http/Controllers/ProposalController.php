<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proposal;
use App\ProposalJob;

class ProposalController extends Controller
{
    
    public function index()
    {
        return Proposal::paginate(10);
    }
    
    public function store(Request $request)
    {   
        if(count($request->jobs) < 2){
            return ['message'=> 'Selecione no minimo dois trabalhos para aceitar a proposta'];
        }
        
        $proposal = Proposal::create($request->all());
         (new MailController)->basic_email((new ProposalJob)->addJobs($request->jobs,$proposal->id));
       return '';
        
    }

  
}
