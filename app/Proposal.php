<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $fillable = ['client_id'];


    public function jobs()
    {
        return $this->belongsToMany('App\ProposalJob', 'proposal_jobs');
    }

}
