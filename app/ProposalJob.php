<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Proposal;
use DB;

class ProposalJob extends Model
{
    protected $table = 'proposal_jobs';
    protected $fillable = ['job_id', 'proposal_id'];

    public function addJobs($jobs_id, $proposal_id)
    {
       
        foreach ($jobs_id as $job_id) {
            
            $proposalJ = new ProposalJob();
            $proposalJ->job_id = intval($job_id);
            $proposalJ->proposal_id = $proposal_id;          
            $proposalJ->save();
           
        }
        
        return DB::table('proposals')->join('proposal_jobs', 'proposals.id', '=', 'proposal_jobs.proposal_id')->where('proposals.id','=', $proposal_id)->get();
        
    }
}
