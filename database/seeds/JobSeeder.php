<?php

use Illuminate\Database\Seeder;
use App\Job;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Job::create([
            'name' => 'Lavador'
        ]);
        Job::create([
            'name' => 'Programador'
        ]);
        Job::create([
            'name' => 'Camiseta'
        ]);
        Job::create([
            'name' => 'Camareira'
        ]);
    }
}
