<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {return view('welcome');});

Route::post('api/login', 'ApiLoginController@login');

Route::middleware('jwt.auth')->group( function () {

    Route::resource('api/client','ClientController');    
    Route::resource('api/job','JobController');
    Route::resource('api/proposal', 'ProposalController');
   
});
